package in.juspay.hdfc;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import in.juspay.hdfc.utils.TOTP;

public class TOTPActivity extends AppCompatActivity {

    private TOTPDisplayTimer totpDisplayTimer;
    private TextView totpDisplay;
    private TextView timer;
    private CounterClass timing;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_totp);
        totpDisplay = (TextView) findViewById(R.id.totp);
        timer = (TextView) findViewById(R.id.timer);
        setTimer();
    }

    protected long getTimer() {
        long time = (System.currentTimeMillis() / 1000L);
        return (5 - ((time / 60) % 5) - 1) * 60 + (60 - (time % 60));
    }

    protected void setTimer() {
        long time = getTimer();
        timer.setText(String.format("%02d:%02d", time/60, time%60));
    }

    @Override
    protected void onResume() {
        super.onResume();
        totpDisplayTimer = (new TOTPDisplayTimer());
        totpDisplayTimer.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_totp, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class TOTPDisplayTimer extends AsyncTask<Void, Void, Void>{

        private TOTP totp;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(totp == null) {
                totpDisplayTimer = (new TOTPDisplayTimer());
            }
            totp = new TOTP();
            try {
                totp.storeToken(getApplicationContext(), "juspay", "1234", "abcdefghijklmnopqrst", "hail_hitler");
                String number = totp.generateTOTP(getApplicationContext(), "juspay", "abcdefghijklmnopqrst", "1234");
                Log.i("TOTP", totpDisplay.getText().toString());
                if(!totpDisplay.getText().toString().equals(number)) {
                    totpDisplay.setText(number);
                    timing = new CounterClass(getTimer()*1000, 1000);
                    timing.start();
                }
                Log.i("TOKEN", totpDisplay.getText().toString());
            } catch (Exception e) {
                Log.e("Error","Some error",e);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            totpDisplayTimer = new TOTPDisplayTimer();
            totpDisplayTimer.execute();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        totpDisplayTimer.cancel(false);
    }

    private class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            String val = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
            timer.setText(val);
        }

        @Override
        public void onFinish() {

        }
    }
}


