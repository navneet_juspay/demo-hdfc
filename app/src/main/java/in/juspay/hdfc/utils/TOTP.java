package in.juspay.hdfc.utils;

import android.content.Context;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.UndeclaredThrowableException;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by newspecies on 05/05/16.
 * https://trivedihardik.wordpress.com/tag/android-aes-example/
 */
public class TOTP {

    private final static String algorithm = "AES";
    private final static String file = "TOKEN";

    public void storeToken(Context context, String clientId, String mpin, String clientAuthToken, String token) throws Exception {
        context.getSharedPreferences(file, Context.MODE_PRIVATE).edit().putString(clientId, encryptToken(mpin, clientAuthToken, token)).commit();
    }

    private String encryptToken(String mpin, String clientAuthToken, String token) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(generateKey(mpin, clientAuthToken), algorithm);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(token.getBytes("UTF-8"));
        return Base64.encodeToString(encrypted, Base64.DEFAULT);
    }

    private String decryptAuthToken(String mpin, String clientAuthToken, String encryptedToken) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(generateKey(mpin, clientAuthToken), algorithm);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(Base64.decode(encryptedToken, Base64.DEFAULT));
        return new String(decrypted, "UTF-8");
    }

    private String fetchToken(Context context, String clientId, String mpin, String clientAuthToken) throws Exception {
        return decryptAuthToken(mpin, clientAuthToken, context.getSharedPreferences(file, Context.MODE_PRIVATE).getString(clientId, ""));
    }

    private byte[] generateKey(String mpin, String clientAuthToken) throws Exception {
        return (mpin+clientAuthToken).getBytes("UTF-8");
    }

    public String generateTOTP(Context context, String clientId, String clientAuthToken, String mpin) throws Exception {
        String token = fetchToken(context, clientId, mpin, clientAuthToken);
        return gen(mpin + clientId + clientAuthToken + token, 6, 2);
    }

    private static byte[] hmac_sha1(String crypto, String keyBytes, String text)
    {
        try {
            Mac hmac;
            hmac = Mac.getInstance(crypto);
            SecretKeySpec macKey = new SecretKeySpec(keyBytes.getBytes(), crypto);
            hmac.init(macKey);
            return hmac.doFinal(text.getBytes());

        } catch (Exception e) {
            throw new UndeclaredThrowableException(e);
        }
    }

    private String gen(String key, int returnDigits, int shaType) throws UnsupportedEncodingException {
        long T0 = 0;
        long X = 300;
        long testTime = (long) (System.currentTimeMillis() / 1000L);

        long T = (testTime - T0) / X;

        String time = Long.toHexString(T).toUpperCase();
        while (time.length() < 16) {
            time = "0" + time;
        }

        String result = null;
        byte[] hash;

        String crypto;
        if (shaType == 0) {
            crypto = "HmacSHA1";
        } else if (shaType == 1) {
            crypto = "HmacSHA256";
        } else {
            crypto = "HmacSHA512";
        }
        hash = hmac_sha1(crypto, key, time);

        int offset = hash[hash.length - 1] & 0xf;

        int binary = ((hash[offset] & 0x7f) << 24)
                | ((hash[offset + 1] & 0xff) << 16)
                | ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

        int otp = binary % (int) (Math.pow(10, returnDigits));

        result = Integer.toString(otp);
        while (result.length() < returnDigits) {
            result = "0" + result;
        }
        return result;
    }
}
