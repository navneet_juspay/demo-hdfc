package in.juspay.hdfc.juspay.in.totp.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import in.juspay.hdfc.utils.TOTP;

/**
 * Created by newspecies on 05/05/16.
 */
public class TOTPAppToAppBroadcastReciver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //This braodcast reciver is app to app

        Intent totpData = new Intent();
        Bundle bundle = intent.getBundleExtra("authParams");

        String clientId = bundle.getString("clientId");
        String authToken = bundle.getString("authToken");
        String mpin = bundle.getString("mpin");

        //totp.setToken("abcdefghijklmnop");
        TOTP totp = new TOTP();
        try {
            totp.generateTOTP(context, clientId, authToken, mpin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
