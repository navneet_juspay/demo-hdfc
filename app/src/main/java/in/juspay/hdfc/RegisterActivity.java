package in.juspay.hdfc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {
    private Button mSubmit;
    private EditText mCRNEditText, mCRN2, mCRN3, mCRN4;
    public ProgressBar mProgressBar;
    private TextView mForgotText;
    private Toolbar bar;
    private boolean notif;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);


        Bundle extras = getIntent().getExtras();
        try {
            notif = extras.getBoolean("notif");
        } catch(NullPointerException e) {
            notif = false;
        }

        mSubmit = (Button) findViewById(R.id.submitButton);
        mCRNEditText = (EditText)findViewById(R.id.crn_edit_text);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        mCRN2 = (EditText)findViewById(R.id.crn_edit_text2);
        mCRN3 = (EditText)findViewById(R.id.crn_edit_text3);
        mCRN4 = (EditText)findViewById(R.id.crn_edit_text4);
        mForgotText = (TextView)findViewById(R.id.forgotText);
        bar = (Toolbar)findViewById(R.id.bar);

        setSupportActionBar(bar);

        mProgressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FF17B186"), PorterDuff.Mode.MULTIPLY);
        mProgressBar.setVisibility(View.INVISIBLE);

        mForgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RegisterActivity.this,
                        "An email has been sent with password reset instructions",Toast.LENGTH_SHORT).show();
            }
        });
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(RegisterActivity.this);
        boolean completed = sp.getBoolean("registered", false);

        if(!completed) {
            Intent intent = new Intent(RegisterActivity.this, OptionsActivity.class);
            startActivity(intent);
            finish();
        }

        mCRNEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCRNEditText.hasFocus() && mCRNEditText.getText().length() == 1){
                    mCRNEditText.setNextFocusDownId(mCRN2.getId());
                }
                if(mCRN2.hasFocus() && mCRN2.getText().length() == 1){
                    mCRN2.setNextFocusDownId(mCRN3.getId());
                }
                if(mCRN3.hasFocus() && mCRN3.getText().length() == 1){
                    mCRN3.setNextFocusDownId(mCRN4.getId());
                }

            }
        });
        if(mCRNEditText.hasFocus() && mCRNEditText.getText().length() == 1){
            mCRNEditText.setNextFocusDownId(mCRN2.getId());
        }
        if(mCRN2.hasFocus() && mCRN2.getText().length() == 1){
            mCRN2.setNextFocusDownId(mCRN3.getId());
        }
        if(mCRN3.hasFocus() && mCRN3.getText().length() == 1){
            mCRN3.setNextFocusDownId(mCRN4.getId());
        }

        mCRNEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer textlength1 = mCRNEditText.getText().length();

                if (textlength1 == 1) {
                    mCRN2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mCRN2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer textlength1 = mCRN2.getText().length();

                if (textlength1 == 1) {
                    mCRN3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mCRN3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer textlength1 = mCRN3.getText().length();

                if (textlength1 == 1) {
                    mCRN4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mCRN4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer textlength1 = mCRN3.getText().length();

                if (textlength1 == 1) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mCRN4.getWindowToken(), 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mCRNEditText, InputMethodManager.SHOW_IMPLICIT);

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBar.setVisibility(View.VISIBLE);
                String CRN = mCRNEditText.getText().toString()+mCRN2.getText().toString()+ mCRN3.getText().toString()+ mCRN4.getText().toString();
                if (CRN.length() < 4) {
                    mProgressBar.setVisibility(View.GONE);
                    Toast.makeText(RegisterActivity.this, "Please enter a valid CRN",
                            Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    Intent intent = new Intent(RegisterActivity.this, PaymentRequestActivity.class);
                    intent.putExtra("notif", notif);
                    startActivity(intent);

                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
