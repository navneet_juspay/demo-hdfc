package in.juspay.hdfc;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentRequestActivity extends AppCompatActivity {

    private static final String URL = "http://shoppingdemonb-env.ap-southeast-1.elasticbeanstalk.com/payment_requests/";
    private RelativeLayout approveButton;
    private TextView denyButton;
    private LinearLayout relativeLayout;
    private TextView merchant;
    private TextView orderId;
    private TextView amount;
    private TextView product;
    private TextView result;
    private Toolbar bar;
    private LinearLayout btns;
    private LinearLayout loader;
    private boolean notif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_request);
        Bundle extras = getIntent().getExtras();
        try {
            notif = extras.getBoolean("notif");
        } catch(NullPointerException e) {
            notif = false;
        }
        bar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(bar);
        bar.setNavigationIcon(R.drawable.arrow);

        if(isNetworkAvailable()) {
            approveButton = (RelativeLayout) findViewById(R.id.approve);
            denyButton = (TextView) findViewById(R.id.deny);
            relativeLayout = (LinearLayout) findViewById(R.id.transactionLayout);
            merchant = (TextView) findViewById(R.id.merchant_value);
            orderId = (TextView) findViewById(R.id.order_value);
            amount = (TextView) findViewById(R.id.amt_value);
            product = (TextView)findViewById(R.id.mobile_value);
            result = (TextView)findViewById(R.id.resultText);
            btns = (LinearLayout)findViewById(R.id.btns);
            loader = (LinearLayout)findViewById(R.id.loader);
            btns.setVisibility(View.GONE);
            loader.setVisibility(View.VISIBLE);


            bar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(PaymentRequestActivity.this, RegisterActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            approveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        loader.setVisibility(View.VISIBLE);
                        btns.setVisibility(View.GONE);
                        updatePaymentRequests("APPROVE");
                    } catch (JSONException e) {
                        Log.i("UPDATE-STATUS", "FAILURE JSON EXCEPTION");
                    }
                }
            });

            denyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        loader.setVisibility(View.VISIBLE);
                        btns.setVisibility(View.GONE);
                        updatePaymentRequests("DECLINE");
                    } catch (JSONException e) {
                        Log.i("UPDATE-STATUS", "FALIED JSON EXCEPTION");
                    }
                }
            });
            fetchPaymentRequests();
        } else {
            Intent intent = new Intent(this, TOTPActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void close() {
        if(!notif) {
            try {
                Toast.makeText(this, "Redirecting to Bank", Toast.LENGTH_LONG).show();
                Thread.sleep(2000);
                finish();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class PaymentRequestChseckTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void fetchPaymentRequests() {
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.i("URL", URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("Response", response.toString());
                            merchant.setText(response.getJSONObject("data").getString("merchant"));
                            amount.setText(response.getJSONObject("data").getString("amount"));
                            product.setText(response.getJSONObject("data").getString("product"));
                            orderId.setText(response.getString("order_id"));
                            loader.setVisibility(View.GONE);
                            btns.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            loader.setVisibility(View.GONE);
                            btns.setVisibility(View.VISIBLE);
                            Log.e("Response-json-exception", e.getMessage(), e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loader.setVisibility(View.GONE);
                        Toast.makeText(PaymentRequestActivity.this, "Something went wrong. Please try again or use TOTP option be disabling your internet", Toast.LENGTH_LONG).show();
                        Log.e("Response-Error", error.getMessage(), error);
                    }
        });
        queue.add(jsonObjectRequest);
    }

    private void updatePaymentRequests(final String status) throws JSONException {
        final RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject responseData = new JSONObject();
        responseData.put("status", status);
        Log.i("URL", URL + "status");
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL+"status", responseData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            denyButton.setVisibility(View.GONE);
                            loader.setVisibility(View.GONE);
                            btns.setVisibility(View.VISIBLE);
                            if(status.equals("APPROVE")) {
                                approveButton.setBackground(getDrawable(R.drawable.transaction_success));
                                result.setTextColor(Color.parseColor("#FF17B186"));
                                result.setText("Approved !");
                            }
                            else {
                                approveButton.setBackground(getDrawable(R.drawable.transaction_declined));
                                result.setTextColor(Color.RED);
                                result.setText("Cancelled");
                            }
                            Log.i("Response", response.toString(4));
                            close();
                        } catch (JSONException e) {
                            Log.e("Response-json-exception", e.getMessage(), e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Response-Error", error.getMessage(), error);
                    }
                });
        queue.add(jsonObjectRequest);
    }

}
