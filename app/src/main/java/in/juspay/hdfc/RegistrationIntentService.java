package in.juspay.hdfc;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {

    private static final String[] TOPICS = {"global"};
    private static final String URL = "http://shoppingdemonb-env.ap-southeast-1.elasticbeanstalk.com/storeKey";
    private String crn;

    public RegistrationIntentService() {
        super("Intent");
    }

    @Override
    public void onHandleIntent(Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);
        crn = intent.getStringExtra("CID");
        String token = null;
        try {
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("veera", "GCM Registration Token: " + token);
        try {
            updateDB(token);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = sp.edit();
            edit.putBoolean("registered", true);
            edit.apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            subscribeTopics(token);
            Intent in = new Intent(this, RegisterActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(in);
            stopSelf();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

    private void updateDB(final String status) throws JSONException {
        final RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject responseData = new JSONObject();
        responseData.put("key", status);
        responseData.put("CRN", "CID_"+crn);
        Log.i("URL", URL);
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, responseData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("Response", response.toString(4));
                        } catch (JSONException e) {
                            Log.e("Response-json-exception", e.getMessage(), e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Response-Error", error.getMessage(), error);
                    }
                });
        queue.add(jsonObjectRequest);
    }
}
