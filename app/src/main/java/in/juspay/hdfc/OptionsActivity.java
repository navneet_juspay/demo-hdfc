package in.juspay.hdfc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class OptionsActivity extends AppCompatActivity {

    private EditText mpin;
    private Button submitButton;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);




        mpin = (EditText) findViewById(R.id.crn_edit_text);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffffff"),
               PorterDuff.Mode.MULTIPLY);
        submitButton = (Button)findViewById(R.id.submitButton);
        progressBar.setVisibility(View.GONE);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mpinText = mpin.getText().toString();
                if(mpin.getText().length() == 8) {
                    progressBar.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mpin.getWindowToken(), 0);

                    Intent in = new Intent(OptionsActivity.this, RegistrationIntentService.class);
                    in.putExtra("CID", mpinText);
                    startService(in);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
